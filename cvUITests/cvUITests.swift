//
//  cvUITests.swift
//  cvUITests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import SDWebImage
@testable import CV

class cvUITests: XCTestCase {

	
	///	Application
	let application = XCUIApplication()
	
	///	Timeout
	let timeout: TimeInterval = 320.0
	
	///	Exist predicate
	let existsPredicate = NSPredicate(format: "exists == 1")
	
	///	Doesnt exist predicate
	let doesntExistPredicate = NSPredicate(format: "exists == 0")
	
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        application.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	/**
		Test the loading of data
	*/
	func testBasicLoading() {
		
		///	Initial state
		let activityVisible = expectation(for: existsPredicate,
										  evaluatedWith: application.otherElements.matching(identifier: UITests.activity.rawValue),
										  handler: nil)
		let collectionHidden = expectation(for: doesntExistPredicate,
										   evaluatedWith: application.collectionViews.element(matching: XCUIElement.ElementType.table, identifier: UITests.table.rawValue),
										   handler: nil)
		
		/// Switched
		let activityHidden = expectation(for: doesntExistPredicate,
										 evaluatedWith: application.otherElements.matching(identifier: UITests.activity.rawValue),
										 handler: nil)
		let collectionVisible = expectation(for: existsPredicate,
											evaluatedWith: application.collectionViews.element(matching: XCUIElement.ElementType.table, identifier: UITests.table.rawValue) ,
											handler: nil)
		
		XCTWaiter().wait(for: [activityVisible, collectionHidden,
							   activityHidden, collectionVisible],
						 timeout: timeout,
						 enforceOrder: true)
	}

}
