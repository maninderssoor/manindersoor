# Maninder Soor

[![Carthage Compatible](https://img.shields.io/badge/Carthage-compatible-4BC51D.svg?style=flat)](https://github.com/Carthage/Carthage)
![Code Coverage](https://img.shields.io/badge/coverage-90%25-green.svg)
[![iOS Platform](https://img.shields.io/badge/platform-ios-lightgrey.svg)](https://img.shields.io/badge/platform-ios-lightgrey.svg)

## Requirments:

In this coding test you will be writing a native mobile application using the latest versions of either Swift or Kotlin.

The app consists of your own CV app. You can pick some ideas from the following list (not limited to this):
* Professional summary.
* Topics of knowledge / technical.
* Past experience: company name, role name, date from - to, main responsibilities, achievements, company logo, etc.
* You can host one/more json/xml file/s on gist.github.com and use it as the host of the data from your app (or any alternative).


## Notes

This sample app uses a hosted JSON file to fetch a CV.

API fetching uses the APIProtocol to create services via a Networking protocol using generics to create the fetch request alongside paramters (APIProtocolParameters).

Data is requested by the view controller where the ListingViewModel fetches from the API using the service above. An adapter converts the API data to DB types.

I've used SDWebImage for caching; mainly for it's cancel/ set image features (and in built caching), though images could be downloaded in the caches directory and stored in a NSCache during app init if done without a library.

3rd Party Libraries:

- SDWebImage: Caching of images with operation queue downloading rather than using 'URLSession'.
- Lottie: UI for loading animation.

Unit testing has been done throughout with code coverage of 90%.

## System Requirements

- iOS 10.0+
- Xcode 10.0+
- Swift 4.0
- Command Line Tools 10.0+

## Documentation

Code has been document to a minumum of:

* Classes
* Functions
* Properties

Documentation can re-generated using jazz docs [Jazzy](https://github.com/realm/jazzy)

## Build

### Instructions

1. Checkout the application via Github or via a terminal.
2. Run `carthage update --platform iOS`
3. Open the application in Xcode and run.

## Author

Maninder Soor (http://manindersoor.com)
