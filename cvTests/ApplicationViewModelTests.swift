//
//  ApplicationViewModelTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	ApplicationViewModelTests
*/
class ApplicationViewModelTests: cvTests {
	
	///	View Model
	private let viewModel = ApplicationViewModel(cv: cvTests.mockCV())
	
	///	First index path
	private let firstIndexPath = IndexPath(row: 0, section: 0)
	
	/**
		Test name
	*/
	func testName() {
		guard let name = viewModel.name(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the name")
			return
		}
		
		XCTAssertEqual(name, "Application 1", "Not correctly set")
	}
	
	/**
		Test imageURL
	*/
	func testImageURL() {
		guard let imageURL = viewModel.imageURL(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the imageURL")
			return
		}
		
		XCTAssertEqual(imageURL, "https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/e0/4c/0f/e04c0f87-86b2-9d66-ae8b-e27c31f9fd42/AppIcon-0-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-7.png/460x0w.jpg", "Not correctly set")
	}
	
	/**
		Test Summary
	*/
	func testSummary() {
		guard let summary = viewModel.summary(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the summary")
			return
		}
		
		XCTAssertEqual(summary, "Summary 1", "Not correctly set")
	}
	
	/**
		Test App Store
	*/
	func testAppStoreURL() {
		guard let appStoreURL = viewModel.appStoreURL(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the appStoreURL")
			return
		}
		
		XCTAssertEqual(appStoreURL, "https://itunes.apple.com/gb/app/arsenal/id332060637?mt=8", "Not correctly set")
	}
}

