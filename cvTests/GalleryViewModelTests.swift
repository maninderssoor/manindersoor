//
//  GalleryViewModelTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	GalleryViewModelTests
*/
class GalleryViewModelTests: cvTests {
	
	///	View Model
	private let viewModel = GalleryViewModel(application: cvTests.application())
	
	///	First index path
	private let firstIndexPath = IndexPath(row: 0, section: 0)
	
	/**
		Test count
	*/
	func testCount() {
		XCTAssertEqual(viewModel.numberOfItems(), 2, "There should be 2 gallery items")
	}
	
	/**
		Test Title
	*/
	func testTitle() {
		guard let title = viewModel.title() else {
			XCTFail("Couldn't get the title")
			return
		}
		
		XCTAssertEqual(title, "Application 1", "Not correctly set")
	}
	
	/**
		Image URL
	*/
	func testImageURL() {
		guard let imageURL = viewModel.imageURL(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the imageURL")
			return
		}
		
		XCTAssertEqual(imageURL, "https://is3-ssl.mzstatic.com/image/thumb/Purple124/v4/22/7b/57/227b575c-5118-4464-3a03-6385aefc86db/pr_source.jpg/460x0w.jpg", "Not correctly set")
	}
}

