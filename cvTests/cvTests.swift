//
//  cvTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

class cvTests: XCTestCase {

	///	Timeout for all asynchronous calls
	public let timeout = 120.0
	
    override func setUp() {}

    override func tearDown() {}

	
	/**
		Returns a CV
	*/
	static func mockAPICV() -> APICV {
		let experience1 = APIExperience(title: "Experience 1", startDate: 1533081600, endDate: 1551312000, organisation: "Organisation", summary: "Summary 1", tags: ["iOS", "Swift"])
		let experience2 = APIExperience(title: "Experience 2", startDate: 1514764800, endDate: 1532995200, organisation: "Organisation 2", summary: "Summary 2", tags: ["Objective-C", "Apple"])
		
		let application1 = APIApplication(name: "Application 1",
										  summary: "Summary 1",
										  logoURL: "https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/e0/4c/0f/e04c0f87-86b2-9d66-ae8b-e27c31f9fd42/AppIcon-0-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-7.png/460x0w.jpg",
										  itunesURL: "https://itunes.apple.com/gb/app/arsenal/id332060637?mt=8",
										  gallery: ["https://is3-ssl.mzstatic.com/image/thumb/Purple124/v4/22/7b/57/227b575c-5118-4464-3a03-6385aefc86db/pr_source.jpg/460x0w.jpg", "https://is2-ssl.mzstatic.com/image/thumb/Purple123/v4/97/2e/49/972e49b1-7d21-26c6-7b10-b84a8bcde5a0/pr_source.jpg/460x0w.jpg"])
		
		let application2 = APIApplication(name: "Application 2",
										  summary: "Summary 2",
										  logoURL: "https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/ac/dd/52/acdd52ae-e8d7-faff-b314-abfd1f7f7fee/AppIcon-0-1x_U007emarketing-0-0-85-220-0-10.png/460x0w.jpg",
										  itunesURL: "https://itunes.apple.com/us/app/f1-tv/id1315007279?ls=1&mt=8",
										  gallery: ["https://is2-ssl.mzstatic.com/image/thumb/Purple118/v4/a9/37/fb/a937fb96-03c0-0cc7-ea41-49f7cf195036/pr_source.jpg/460x0w.jpg", "https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/6f/e0/cd/6fe0cd89-372e-f8c1-7d17-48e6b7186df6/pr_source.jpg/460x0w.jpg"])
		
		let cv = APICV(name: "Name",
					   website: "Website",
					   linkedIn: "LinkedIn",
					   summary: "Summary",
					   skills: ["Skill 1", "Skill 2", "Skill 3"],
					   experience: [experience1, experience2],
					   applications: [application1, application2])
		
		return cv
	}
	
	/**
		Returns a DB CV
	*/
	static func mockCV() -> CV {
		
		let experience1Tag = Tag(name: "iOS")
		let experience1Tag2 = Tag(name: "Swift")
		let experience1 = Experience(title: "Experience 1",
									 startDate: Date(timeIntervalSince1970: TimeInterval(1533081600)),
									 endDate: Date(timeIntervalSince1970: TimeInterval(1551312000)),
									 organisation: "Organisation",
									 summary: "Summary 1",
									 tags: [experience1Tag, experience1Tag2])
		
		let experience2Tag = Tag(name: "Objective-C")
		let experience2Tag2 = Tag(name: "Apple")
		let experience2 = Experience(title: "Experience 1",
									 startDate: Date(timeIntervalSince1970: TimeInterval(1514764800)),
									 endDate: Date(timeIntervalSince1970: TimeInterval(1532995200)),
									 organisation: "Organisation 2",
									 summary: "Summary 2",
									 tags: [experience2Tag, experience2Tag2])
		
		let mediaApplication1 = Media(urlString: "https://is3-ssl.mzstatic.com/image/thumb/Purple124/v4/22/7b/57/227b575c-5118-4464-3a03-6385aefc86db/pr_source.jpg/460x0w.jpg", type: .image)
		let mediaApplication2 = Media(urlString: "https://is2-ssl.mzstatic.com/image/thumb/Purple123/v4/97/2e/49/972e49b1-7d21-26c6-7b10-b84a8bcde5a0/pr_source.jpg/460x0w.jpg", type: .image)
		let application1 = Application(name: "Application 1",
									   summary: "Summary 1",
									   logoURL: "https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/e0/4c/0f/e04c0f87-86b2-9d66-ae8b-e27c31f9fd42/AppIcon-0-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-7.png/460x0w.jpg",
									   itunesURL: "https://itunes.apple.com/gb/app/arsenal/id332060637?mt=8",
									   gallery: [mediaApplication1, mediaApplication2])
		
		
		let mediaApplication3 = Media(urlString: "https://is2-ssl.mzstatic.com/image/thumb/Purple118/v4/a9/37/fb/a937fb96-03c0-0cc7-ea41-49f7cf195036/pr_source.jpg/460x0w.jpg", type: .image)
		let mediaApplication4 = Media(urlString: "https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/6f/e0/cd/6fe0cd89-372e-f8c1-7d17-48e6b7186df6/pr_source.jpg/460x0w.jpg", type: .image)
		
		let application2 = Application(name: "Application 2",
									   summary: "Summary 2",
									   logoURL: "https://is1-ssl.mzstatic.com/image/thumb/Purple113/v4/ac/dd/52/acdd52ae-e8d7-faff-b314-abfd1f7f7fee/AppIcon-0-1x_U007emarketing-0-0-85-220-0-10.png/460x0w.jpg",
									   itunesURL: "https://itunes.apple.com/us/app/f1-tv/id1315007279?ls=1&mt=8",
									   gallery: [mediaApplication3, mediaApplication4])
		
		let tag1 = Tag(name: "Skill 1")
		let tag2 = Tag(name: "Skill 2")
		let tag3 = Tag(name: "Skill 3")
		
		let cv = CV(name: "Name",
					website: "Website",
					linkedIn: "LinkedIn",
					summary: "Summary",
					skills: [tag1, tag2, tag3],
					experience: [experience1, experience2],
					applications: [application1, application2])
		
		return cv
	}
	
	/**
		Returns an applciation
	*/
	static func application() -> Application {
		let mediaApplication1 = Media(urlString: "https://is3-ssl.mzstatic.com/image/thumb/Purple124/v4/22/7b/57/227b575c-5118-4464-3a03-6385aefc86db/pr_source.jpg/460x0w.jpg", type: .image)
		let mediaApplication2 = Media(urlString: "https://is2-ssl.mzstatic.com/image/thumb/Purple123/v4/97/2e/49/972e49b1-7d21-26c6-7b10-b84a8bcde5a0/pr_source.jpg/460x0w.jpg", type: .image)
		let application1 = Application(name: "Application 1",
									   summary: "Summary 1",
									   logoURL: "https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/e0/4c/0f/e04c0f87-86b2-9d66-ae8b-e27c31f9fd42/AppIcon-0-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-7.png/460x0w.jpg",
									   itunesURL: "https://itunes.apple.com/gb/app/arsenal/id332060637?mt=8",
									   gallery: [mediaApplication1, mediaApplication2])
		
		return application1
	}
}
