//
//  File.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	A Mock Network that returns empty results
*/
final class MockEmptyNetwork: Networking {
	
	/**
		Returns an array of Clothing items
	*/
	func fetch<E>(with type: E.Type, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol {
		
		let service = APICVService(cvs: [])
		completion(APIProtocolCompletion(isSuccess: true, service: service))
	}
	
}
