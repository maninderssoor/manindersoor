//
//  UserViewModelTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	CV View Model Tests
*/
final class CVViewModelTests: cvTests {
	
	///	A CV View Model
	let viewModel = CVViewModel(networkingManager: MockNetwork())
	
	///	Empty CV view model
	let emptyViewModel = CVViewModel(networkingManager: MockEmptyNetwork())
	
	/**
		Test Listing view controller data
	*/
	func testListingCount() {
		let asychronousExpectation = expectation(description: "testListingData")
		
		viewModel.loadData {[weak self](isSuccess) in
			XCTAssertNotNil(self, "Self should not be nil")
			XCTAssertTrue(isSuccess, "The listing view model should be a success")

			guard let self = self else {
				XCTFail("No weak self. Already been checked. What!")
				asychronousExpectation.fulfill()
				return
			}
			
			XCTAssertNotNil(self.viewModel.cv, "The CV shouldn't be nil now")
			
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}

}
