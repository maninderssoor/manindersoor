//
//  SkillsViewModelTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	SummaryViewModelTests
*/
class SkillsViewModelTests: cvTests {
	
	///	View Model
	private let viewModel = SkillsViewModel(cv: cvTests.mockCV())
	
	/**
		Test skills count
	*/
	func testCount() {
		XCTAssertEqual(viewModel.skills().count, 3, "There should be 3 skills")
	}
	
	/**
		Test Skills being set
	*/
	func testSkillsSet() {
		guard viewModel.skills().count == 3 else {
			XCTFail("Incorrect number of skills")
			return
		}
		
		XCTAssertEqual(viewModel.skills()[0].name, "Skill 1", "Not correctly set")
		XCTAssertEqual(viewModel.skills()[1].name, "Skill 2", "Not correctly set")
		XCTAssertEqual(viewModel.skills()[2].name, "Skill 3", "Not correctly set")
	}
}

