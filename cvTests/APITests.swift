//
//  APITests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	API Tests, to check services are running correctly
*/
class APITests: cvTests {
	
	/// A networking manager
	let networkingManager = NetworkManager()
	
	///	A Mock network manager
	let mockNetworkManager = MockNetwork()
	
	/**
		Test connections API Call
	*/
	 func testConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testConnectionsAPICall")
		
		networkingManager.fetch(with: APICVService.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.service, "The service returned should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking protocol
	*/
	public func testNetworkProtocol() {
		let asychronousExpectation = expectation(description: "testNetworkProtocol")
		
		mockNetworkManager.fetch(with: APICVService.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertNotNil(results.service, "The service returned should not be nil")
			
			guard let items = results.service as? APICVService else {
				XCTFail("Error unwrapping the service as a Clothing Service")
				asychronousExpectation.fulfill()
				return
			}
			
			XCTAssertEqual(items.cvs.count, 1, "There should be 1 items of clothing from the Mock response")
			
			let firstItem = items.cvs[0]
			XCTAssertEqual(firstItem.name, "Name", "The name wasn't set correctly")
			XCTAssertEqual(firstItem.website, "Website", "The website wasn't set correctly")
			XCTAssertEqual(firstItem.linkedIn, "LinkedIn", "The LinkedIn wasn't set correctly")
			XCTAssertEqual(firstItem.summary, "Summary", "The summary wasn't set correctly")
			
			XCTAssertEqual(firstItem.skills.count, 3, "There should be 4 skills")
			XCTAssertEqual(firstItem.skills[0], "Skill 1", "This skill hasn't been set correctly")
			XCTAssertEqual(firstItem.skills[1], "Skill 2", "This skill hasn't been set correctly")
			XCTAssertEqual(firstItem.skills[2], "Skill 3", "This skill hasn't been set correctly")
			
			XCTAssertEqual(firstItem.experience.count, 2, "There should be 2 experiences")
			
			XCTAssertEqual(firstItem.applications.count, 2, "There should be 2 applications")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test connections no url string API Call
	*/
	public func testNoURLConnectionsAPICall() {
		let asychronousExpectation = expectation(description: "testNoURLConnectionsAPICall")
		
		networkingManager.fetch(with: APITestNoURL.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
	/**
		Test networking manager bad API
	*/
	public func testBadAPICall() {
		let asychronousExpectation = expectation(description: "testBadAPICall")
		
		networkingManager.fetch(with: APITestBadURL.self) { (results) in
			
			XCTAssertNotNil(results, "The results should not be nil")
			XCTAssertFalse(results.isSuccess, "The call shouldn't have been a success")
			
			asychronousExpectation.fulfill()
		}
		
		// Wait the API call to respond
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error with the API call \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
	
}


/**
	No URL
*/
public struct APITestNoURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "NO URL"
	
	/// The URL for this call
	static public var urlString: String = ""
}


/**
	Fetch connection API
*/
public struct APITestBadURL: APIProtocol {
	
	/// The name of this API call, for logging
	static public var title: String = "BAD URL"
	
	/// The URL for this call
	static public var urlString: String = "http://someurlthatwontwork.com"
}
