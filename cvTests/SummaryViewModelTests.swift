//
//  SummaryViewModelTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	SummaryViewModelTests
*/
class SummaryViewModelTests: cvTests {

	///	View Model
	private let summaryViewModel = SummaryViewModel(cv: cvTests.mockCV())
	
	/**
		Test name
	*/
	func testListingName() {
		XCTAssertEqual(summaryViewModel.name(), cvTests.mockCV().name, "The name isn't correct")
	}
	
	/**
		Test website
	*/
	func testListingWebsite() {
		XCTAssertEqual(summaryViewModel.website(), cvTests.mockCV().website, "The website isn't correct")
	}
	
	/**
		Test linkedIn
	*/
	func testLinkedIn() {
		XCTAssertEqual(summaryViewModel.linkedIn(), cvTests.mockCV().linkedIn, "The LinkedIn isn't correct")
	}
	
	/**
		Test Summary
	*/
	func testSummary() {
		XCTAssertEqual(summaryViewModel.summary(), cvTests.mockCV().summary, "The summary isn't correct")
	}
}
