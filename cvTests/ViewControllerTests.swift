//
//  ViewControllerTests.swift
//  MovieDBTests
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
import Lottie
@testable import CV

/**
	View Controller tests
*/
class ViewControllerTests: cvTests {
	
	///	Identifier
	let bundleID = "com.manindersoor.cv"
	
	///	Storyboard
	let storyboard = UIStoryboard(name: GalleryViewController.identifier, bundle: Bundle(identifier: "com.manindersoor.cv"))
	
	///	View Model
	let galleryViewModel = GalleryViewModel(application: cvTests.application())
	
	/**
		Fetch the controller
	*/
	private func controller() -> GalleryViewController? {
		
		guard let controller = storyboard.instantiateInitialViewController() as? GalleryViewController else {
			return nil
		}
		
		controller.viewDidLoad()
		let _ = controller.view
		
		return controller
	}
	
	/**
		Test setup
	*/
	func testSetup() {
		guard let controller = controller() else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		
		XCTAssertNil(controller.viewModel, "The view model should be nil at this point")
		controller.setup(withApplication: cvTests.application())
		XCTAssertNotNil(controller.viewModel, "The view model shouldn't be nil now")
	}
	
	/**
		Test navigation title
	*/
	func testNavigationTitle() {
		guard let controller = controller() else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		
		controller.setup(withApplication: cvTests.application())
		if let navigationTitle = controller.navigationItem.title {
			XCTAssertEqual(navigationTitle, "Application 1", "The navigation title wasn't set correctly")
		} else {
			XCTFail("There should be a navigation title")
		}
	}
	
	/**
		Test collection view items
	*/
	func testCollectionViewItems() {
		guard let controller = controller() else {
			XCTFail("The controller wasn't initialised correctly")
			return
		}
		guard let collection = controller.collection else {
			XCTFail("The collection wasn't initialised correctly")
			return
		}
		
		XCTAssertEqual(collection.numberOfItems(inSection: 0), 0, "Should be 0 right now, no application set")
		controller.setup(withApplication: cvTests.application())
		controller.viewDidLoad()
		let _ = controller.collectionView(collection, cellForItemAt: IndexPath(item: 0, section: 0))
		XCTAssertEqual(collection.numberOfItems(inSection: 0), 2, "There should only be one application")
	}
	
	/**
		Test gallery cell setup
	*/
	public func testGalleryCellSetup() {
		guard let xib = UINib(nibName: GalleryCell.identifier, bundle: Bundle(identifier: bundleID)).instantiate(withOwner: nil, options: nil).first as? GalleryCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		
		xib.awakeFromNib()
		XCTAssertNotNil(xib.activity, "The activity should have been initialised now.")
	}
	
	/**
		Test gallery prepare for resuse
	*/
	public func testGalleryPrepareForReuse() {
		guard let xib = UINib(nibName: GalleryCell.identifier, bundle: Bundle(identifier: bundleID)).instantiate(withOwner: nil, options: nil).first as? GalleryCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		
		xib.prepareForReuse()
		
		guard let image = xib.imageApplication else {
			XCTFail("Couldn't initialise the application image vie")
			return
		}
		XCTAssertNil(image.image, "The image view shouldn't have an image")
		XCTAssertEqual(image.alpha, 0.0, "The image view should be hidden")
		
		
		guard let activity = xib.activity else {
			XCTFail("Couldn't initialise the activity")
			return
		}
		XCTAssertEqual(activity.alpha, 1.0, "The activity animation should be visible")
	}
	
	/**
		Test set image
	*/
	public func testSetImage() {
		let asychronousExpectation = expectation(description: "testNetworkProtocol")
		guard let xib = UINib(nibName: GalleryCell.identifier, bundle: Bundle(identifier: bundleID)).instantiate(withOwner: nil, options: nil).first as? GalleryCell else {
			XCTFail("Couldn't initalise the nib view")
			return
		}
		
		xib.setImage { (_) in
			
			if let imageView = xib.imageApplication {
				XCTAssertEqual(imageView.alpha, 1.0, "The image view should be visible")
			} else {
				XCTFail("No Image View")
				asychronousExpectation.fulfill()
			}
			
			if let activity = xib.activity {
				XCTAssertEqual(activity.alpha, 0.0, "The activity should be hidden")
			} else {
				XCTFail("No Activity")
				asychronousExpectation.fulfill()
			}
			asychronousExpectation.fulfill()
		}
		
		waitForExpectations(timeout: timeout) { (error) in
			if let error = error {
				print("There was an error \(String(describing: error))")
				XCTFail("The asychronousExpectation timed out")
			}
		}
	}
}
