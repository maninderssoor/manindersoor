//
//  ExperienceViewModelTests.swift
//  cvTests
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	ExperienceViewModelTests
*/
class ExperienceViewModelTests: cvTests {
	
	///	View Model
	private let viewModel = ExperienceViewModel(cv: cvTests.mockCV())
	
	///	First index path
	private let firstIndexPath = IndexPath(row: 0, section: 0)
	
	/**
		Test first title being set
	*/
	func testTitle() {
		guard let title = viewModel.title(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the title")
			return
		}
		
		XCTAssertEqual(title, "Experience 1", "Not correctly set")
	}
	
	/**
		Test date being set
	*/
	func testDate() {
		guard let date = viewModel.date(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the date")
			return
		}
		
		XCTAssertEqual(date, "08/2018 - 02/2019", "Not correctly set")
	}
	
	/**
		Test organisation
	*/
	func testOrganisation() {
		guard let organisation = viewModel.organisation(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the organisation")
			return
		}
		
		XCTAssertEqual(organisation, "Organisation", "Not correctly set")
	}
	
	/**
		Test Summary
	*/
	func testSummary() {
		guard let summary = viewModel.summary(atIndexPath: firstIndexPath) else {
			XCTFail("Couldn't get the summary")
			return
		}
		
		XCTAssertEqual(summary, "Summary 1", "Not correctly set")
	}
	
}
