//
//  MockNetwork.swift
//  Net-a-PorterTests
//
//  Created by Maninder Soor on 13/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import XCTest
@testable import CV

/**
	A Mock Network
*/
final class MockNetwork: Networking {
	
	
	/**
		Returns an array of Mock items
	*/
	func fetch<E>(with type: E.Type, completion: @escaping ((APIProtocolCompletion) -> Void)) where E : APIProtocol {
		
		
		let cv = cvTests.mockAPICV()
		let service = APICVService(cvs: [cv])
		
		completion(APIProtocolCompletion(isSuccess: true, service: service))
	}
	
}
