//
//  SummaryViewModel.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Present summary information
*/
struct SummaryViewModel {
	
	///	A CV
	private let cv: CV
	
	// MARK: Methods
	
	/**
		Initialisers
	*/
	init(cv: CV) {
		self.cv = cv
	}
	
	/**
		Get name
	*/
	func name() -> String {
		return cv.name
	}
	
	/**
		Get website
	*/
	func website() -> String {
		return cv.website
	}
	
	/**
		Get Linked In Link
	*/
	func linkedIn() -> String {
		return cv.linkedIn
	}
	
	/**
		Get the summary
	*/
	func summary() -> String {
		return cv.summary
	}
}
