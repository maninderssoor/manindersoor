//
//  GalleryViewModel.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Gallery Information
*/
struct GalleryViewModel {
	
	///	A CV
	private let application: Application
	
	// MARK: Methods
	
	/**
		Initialisers
	*/
	init(application: Application) {
		self.application = application
	}

	/**
		Title
	*/
	func title() -> String? {
		return application.name
	}
	
	/**
		Number of items
	*/
	func numberOfItems() -> Int {
		return application.gallery.count
	}

	/**
		Get the gallery imageURL at index path
	*/
	func imageURL(atIndexPath indexPath: IndexPath) -> String? {
		guard indexPath.row < application.gallery.count else {
			return nil
		}
		
		return application.gallery[indexPath.row].urlString
	}
	
}
