//
//  MovieListingAdapter.swift
//  MovieDB
//
//  Created by Maninder Soor on 06/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	The CV Adapter converts APICV into an array of CV
*/
final class CVAdapter {
	
	// MARK: Constants
	
	///	An APICV
	private let cv: APICV
	
	/**
		Initialiser
	*/
	init(cv: APICV) {
		self.cv = cv
	}
	
	/**
		Converts data and returns an CV
	*/
	func convert() -> CV {
		let skills = self.cv.skills.compactMap({ Tag(name: $0) })
		var experiences = [Experience]()
		var applications = [Application]()
		
		for thisExperience in self.cv.experience {
			let tags = thisExperience.tags.compactMap({ Tag(name: $0) })
			let startDate = Date(timeIntervalSince1970: TimeInterval(thisExperience.startDate))
			let endDate = Date(timeIntervalSince1970: TimeInterval(thisExperience.endDate))
			
			let experience = Experience(title: thisExperience.title,
										startDate: startDate,
										endDate: endDate,
										organisation: thisExperience.organisation,
										summary: thisExperience.summary,
										tags: tags)
			experiences.append(experience)
		}
		
		for thisApplication in self.cv.applications {
			let gallery = thisApplication.gallery.compactMap({ Media(urlString: $0, type: .image) })
			let application = Application(name: thisApplication.name,
										  summary: thisApplication.summary,
										  logoURL: thisApplication.logoURL,
										  itunesURL: thisApplication.itunesURL,
										  gallery: gallery)
			applications.append(application)
		}
		
		let cv = CV(name: self.cv.name,
					website: self.cv.website,
					linkedIn: self.cv.linkedIn,
					summary: self.cv.summary,
					skills: skills,
					experience: experiences,
					applications: applications)
		
		return cv
	}
}


