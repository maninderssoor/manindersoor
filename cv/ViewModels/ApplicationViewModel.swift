//
//  ApplicationViewModel.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Applications Information
*/
struct ApplicationViewModel {
	
	///	A CV
	private let cv: CV
	
	// MARK: Methods
	
	/**
		Initialisers
	*/
	init(cv: CV) {
		self.cv = cv
	}
	
	/**
		Get the application at index path
	*/
	func application(atIndexPath indexPath: IndexPath) -> Application? {
		guard indexPath.row < cv.applications.count else {
			return nil
		}
		
		return cv.applications[indexPath.row]
	}
	
	/**
		Get name
	*/
	func name(atIndexPath indexPath: IndexPath) -> String? {
		guard let application = application(atIndexPath: indexPath) else {
			print("Couldn't get application at index path \(String(describing: indexPath))")
			return nil
		}
		
		return application.name
	}
	
	/**
		Get imageURL
	*/
	func imageURL(atIndexPath indexPath: IndexPath) -> String? {
		guard let application = application(atIndexPath: indexPath) else {
			print("Couldn't get application at index path \(String(describing: indexPath))")
			return nil
		}
		
		return application.logoURL
	}
	
	/**
		Get summary
	*/
	func summary(atIndexPath indexPath: IndexPath) -> String? {
		guard let application = application(atIndexPath: indexPath) else {
			print("Couldn't get application at index path \(String(describing: indexPath))")
			return nil
		}
		
		return application.summary
	}
	
	/**
		Get App Store URL
	*/
	func appStoreURL(atIndexPath indexPath: IndexPath) -> String? {
		guard let application = application(atIndexPath: indexPath) else {
			print("Couldn't get application at index path \(String(describing: indexPath))")
			return nil
		}
		
		return application.itunesURL
	}
	
}
