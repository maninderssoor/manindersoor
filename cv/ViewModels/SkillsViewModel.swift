//
//  SkillsViewModel.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Skills Information
*/
struct SkillsViewModel {
	
	///	A CV
	private let cv: CV
	
	// MARK: Methods
	
	/**
		Initialisers
	*/
	init(cv: CV) {
		self.cv = cv
	}
	
	/**
		Get Skills
	*/
	func skills() -> [Tag] {
		return cv.skills
	}
	
}
