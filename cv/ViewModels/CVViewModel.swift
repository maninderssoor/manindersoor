//
//  ListingViewModel.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	Sections for a CV item
*/
enum CVSection: Int {
	case summary = 0
	case skills = 1
	case experience = 2
	case applications = 3
	
	static var items: Int { return 4 }
}

/**
	The CV View Model handles
*/
final class CVViewModel {
	
	///	A network manager
	private let networkManager: Networking
	
	/// CV
	var cv: CV?
	
	// MARK: Methods
	
	/**
		Initialiser
	*/
	init(networkingManager: Networking = NetworkManager()) {
		self.networkManager = networkingManager
	}
	
	/**
		Load data from the API
	*/
	func loadData(completion: ((Bool) -> Void)? = nil) {
		networkManager.fetch(with: APICVService.self) { [weak self](results) in
			
			guard let self = self, let service = results.service as? APICVService else {
				print("Error parsing the results or no weak self")
				completion?(false)
				return
			}
			
			///	For now we're handing one level, so hardcoded, but opens for multiples from the API means we can scale this later.
			guard let firstCV = service.cvs.first else {
				print("Error getting the first CV")
				completion?(false)
				return
			}
			
			///	Use an Adapter to map API -> DB models and update the DB
			let adapter = CVAdapter(cv: firstCV)
			let cv = adapter.convert()
			self.cv = cv
			completion?(true)
		}
	}
	
	// MARK: UITableView Items
	
	/**
		Number of sections
	*/
	func numberOfSections() -> Int {
		return CVSection.items
	}
	
	/**
		Number of items
	*/
	func numberOfItems(inSection section: Int) -> Int {
		switch section {
		case CVSection.summary.rawValue:
			return 1
		case CVSection.skills.rawValue:
			if let cv = cv, cv.skills.count > 0 {
				return 1
			}
			return 0
		case CVSection.experience.rawValue:
			return cv?.experience.count ?? 0
		case CVSection.applications.rawValue:
			return cv?.applications.count ?? 0
		default:
			return 0
		}
	}
	
	/**
		Title for section
	*/
	func title(forSection section: Int) -> String {
		switch section {
		case CVSection.summary.rawValue:
			return "SUMMARY"
		case CVSection.skills.rawValue:
			return "SKILLS"
		case CVSection.experience.rawValue:
			return "EXPERIENCE"
		case CVSection.applications.rawValue:
			return "APPLICATIONS"
		default:
			return ""
		}
	}
	
	/**
		Height for heade
	*/
	func heightForHeader() -> CGFloat {
		return 70.0
	}
	
	/**
		Fetches the cv
	*/
	func fetchCV() -> CV? {
		return self.cv
	}
	
}
