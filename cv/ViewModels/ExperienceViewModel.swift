//
//  ExperienceViewModel.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Experiences Information
*/
struct ExperienceViewModel {
	
	///	A CV
	private let cv: CV
	
	///	A date formatter
	private let dateFormatter: DateFormatter
	
	// MARK: Methods
	
	/**
		Initialisers
	*/
	init(cv: CV) {
		self.cv = cv
		self.dateFormatter = DateFormatter()
		self.dateFormatter.dateFormat = "MM/YYYY"
	}
	
	/**
		Get the experience at index path
	*/
	private func experience(atIndexPath indexPath: IndexPath) -> Experience? {
		guard indexPath.row < cv.experience.count else {
			return nil
		}
		
		return cv.experience[indexPath.row]
	}
	
	/**
		Get Title at Index Path
	*/
	func title(atIndexPath indexPath: IndexPath) -> String? {
		guard let experience = experience(atIndexPath: indexPath) else {
			print("Couldn't get experience at index path \(String(describing: indexPath))")
			return nil
		}
		
		return experience.title
	}
	
	/**
		Get Dates at Index Path
	*/
	func date(atIndexPath indexPath: IndexPath) -> String? {
		guard let experience = experience(atIndexPath: indexPath) else {
			print("Couldn't get experience at index path \(String(describing: indexPath))")
			return nil
		}
		let startDate = dateFormatter.string(from: experience.startDate)
		let endDate = dateFormatter.string(from: experience.endDate)
		
		return "\(startDate) - \(endDate)"
	}
	
	/**
		Get Organisation at Index Path
	*/
	func organisation(atIndexPath indexPath: IndexPath) -> String? {
		guard let experience = experience(atIndexPath: indexPath) else {
			print("Couldn't get experience at index path \(String(describing: indexPath))")
			return nil
		}
		
		return experience.organisation
	}
	
	/**
		Get Summary at Index Path
	*/
	func summary(atIndexPath indexPath: IndexPath) -> String? {
		guard let experience = experience(atIndexPath: indexPath) else {
			print("Couldn't get experience at index path \(String(describing: indexPath))")
			return nil
		}
		
		return experience.summary
	}
	
}
