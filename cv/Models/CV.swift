//
//  Movie.swift
//  MovieDB
//
//  Created by Maninder Soor on 06/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	A CV type
*/
struct CV {
	
	///	The name
	let name: String
	
	///	Website of this person
	let website: String
	
	///	LinkedIn Link for this person
	let linkedIn: String
	
	///	A summary of the person
	let summary: String
	
	///	An array of skills
	let skills: [Tag]
	
	///	Experiences
	let experience: [Experience]
	
	///	Applications
	let applications: [Application]
	
	/**
		Initialiser
	*/
	init(name: String, website: String, linkedIn: String, summary: String, skills: [Tag], experience: [Experience], applications: [Application]) {
		self.name = name
		self.website = website
		self.linkedIn = linkedIn
		self.summary = summary
		self.skills = skills
		self.experience = experience
		self.applications = applications
	}
}
