//
//  Media.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Media type
*/
struct Media {
	
	///	The url of the media item
	let urlString: String
	
	///	The type of media
	let type: String
	
	/**
		Initialiser
	*/
	init(urlString: String, type: MediaType) {
		self.urlString = urlString
		self.type = type.rawValue
	}
}


enum MediaType: String {
	case image
}
