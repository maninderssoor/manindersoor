//
//  Experience.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	An Experience type
*/
struct Experience {
	
	///	The title held
	let title: String
	
	///	The start date
	let startDate: Date
	
	///	The end date
	let endDate: Date
	
	///	Name of the organisation/ company
	let organisation: String
	
	///	Summary of experience
	let summary: String
	
	///	Tags
	let tags: [Tag]
	
	/**
		Initialiser
	*/
	init(title: String, startDate: Date, endDate: Date, organisation: String, summary: String, tags: [Tag]) {
		self.title = title
		self.startDate = startDate
		self.endDate = endDate
		self.organisation = organisation
		self.summary = summary
		self.tags = tags
	}
}
