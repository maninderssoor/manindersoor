//
//  Application.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	An Application type
*/
struct Application {
	
	///	The name of the application
	let name: String
	
	///	Summary of the application
	let summary: String
	
	///	Logo url for the application
	let logoURL: String
	
	///	iTunes Link for the application
	let itunesURL: String
	
	///	An array of Gallery URLs
	let gallery: [Media]
	
	/**
		Initialiser
	*/
	init(name: String, summary: String, logoURL: String, itunesURL: String, gallery: [Media]) {
		self.name = name
		self.summary = summary
		self.logoURL = logoURL
		self.itunesURL = itunesURL
		self.gallery = gallery
	}
}
