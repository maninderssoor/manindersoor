//
//  Tag.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Tag Type
*/
struct Tag {
	
	///	The name of the tag
	let name: String
	
	/**
		Initialiser
	*/
	init(name: String) {
		self.name = name
	}
}
