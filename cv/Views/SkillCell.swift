//
//  SkillCell.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	SkillCell
*/
final class SkillCell: UITableViewCell {
	
	// MARK: Identifier
	static let identifier = "SkillCell"
	
	// MARK: Outlets
	
	///	A collection of skills
	@IBOutlet weak var collectionSkills: UICollectionView?
	
	// MARK: Setup
	
	/**
		Register cell
	*/
	override func awakeFromNib() {
		super.awakeFromNib()
		
		collectionSkills?.register(UINib(nibName: TagCollectionCell.identifier, bundle: nil), forCellWithReuseIdentifier: TagCollectionCell.identifier)
	}
}
