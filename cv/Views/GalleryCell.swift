//
//  GalleryCell.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import Lottie
import UIKit

/**
	TagCollectionCell
*/
final class GalleryCell: UICollectionViewCell {
	
	// MARK: Identifier
	static let identifier = "GalleryCell"
	
	// MARK: Outlets
	
	///	Animation view
	var activity: AnimationView?
	
	///	The image
	@IBOutlet weak var imageApplication: UIImageView?
	
	// MARK: Methods
	
	/**
		Setup animation view
	*/
	override func awakeFromNib() {
		super.awakeFromNib()
		
		if activity == nil {
			activity = AnimationView(animation: Animation.named(Constants.activity.rawValue))
			activity?.contentMode = .scaleAspectFit
			activity?.frame = imageApplication?.frame ?? contentView.frame
			
			if let unwrappedActivityView = activity {
				contentView.addSubview(unwrappedActivityView)
			}
			activity?.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		}
	}
	
	/**
		Reset
	*/
	override func prepareForReuse() {
		super.prepareForReuse()
		
		imageApplication?.image = nil
		imageApplication?.alpha = 0.0
		activity?.alpha = 1.0
	}
	
	/**
		Set image
	*/
	func setImage(completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, animations: {[weak self] in
			self?.imageApplication?.alpha = 1.0
			self?.activity?.alpha = 0.0
		}) { (isComplete) in
			completion?(isComplete)
		}
	}
}
