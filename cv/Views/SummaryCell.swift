//
//  MovieCell.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	SummaryCell
*/
final class SummaryCell: UITableViewCell {
	
	// MARK: Identifier
	static let identifier = "SummaryCell"
	
	// MARK: Outlets
	
	///	The name
	@IBOutlet weak var labelName: UILabel?
	
	///	The website label
	@IBOutlet weak var labelWebsite: UITextView?
	
	///	LinkedIn Link
	@IBOutlet weak var labelLinkedIn: UITextView?
	
	///	Summary label
	@IBOutlet weak var labelSummary: UILabel?
	
	// MARK: Setup
	
	/**
		Prepare for reuse
	*/
	override func prepareForReuse() {
		super.prepareForReuse()
		
		labelName?.text = ""
		labelWebsite?.text = ""
		labelLinkedIn?.text = ""
		labelSummary?.text = ""
	}
}
