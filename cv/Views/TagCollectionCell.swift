//
//  TagCollectionCell.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	TagCollectionCell
*/
final class TagCollectionCell: UICollectionViewCell {
	
	// MARK: Identifier
	static let identifier = "TagCollectionCell"
	
	// MARK: Outlets
	
	///	The tag
	@IBOutlet weak var labelTag: UILabel?
	
	// MARK: Methods
	
	/**
		Corner radius
	*/
	override func awakeFromNib() {
		super.awakeFromNib()
		
		labelTag?.layer.cornerRadius = 6.0
		labelTag?.layer.masksToBounds = true
	}
}
