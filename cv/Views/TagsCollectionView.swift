//
//  SkillsCollectionView.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	A Tags Collection View
*/
class TagsDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	// MARK: Private Variables
	
	///	Tags
	private let tags: [Tag]
	
	/**
		Initialiser
	*/
	init(tags: [Tag]) {
		self.tags = tags
	}
	
	// MARK: UICollectionViewDataSource
	
	/**
		Number of items
	*/
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
	
	/**
		Items at index path
	*/
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return tags.count
	}
	
	/**
		Cell for index path
	*/
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCollectionCell.identifier, for: indexPath) as? TagCollectionCell else {
			preconditionFailure("Couldn't dequeue TagCollectionCell at index Path \(String(describing: indexPath))")
		}
		guard indexPath.item < tags.count else {
			return cell
		}
		
		cell.labelTag?.text = tags[indexPath.item].name
		
		return cell
	}
	
	/**
		Size for item
	*/
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: 100.0, height: 70.0)
	}
}
