//
//  ApplicationCell.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit
import Lottie

/**
	ApplicationCell
*/
final class ApplicationCell: UITableViewCell {
	
	// MARK: Identifier
	static let identifier = "ApplicationCell"
	
	// MARK: Variables
	
	///	Animation view
	private var activity: AnimationView?
	
	// MARK: Outlets
	
	///	The image view for the pplication
	@IBOutlet weak var imageApplication: UIImageView?
	
	///	The name of the application
	@IBOutlet weak var labelName: UILabel?
	
	///	The Summary for the application
	@IBOutlet weak var labelSummary: UILabel?
	
	///	The App Store Link
	@IBOutlet weak var labelAppStoreLink: UITextView?
	
	// MARK: Setup
	
	/**
		Prepare for reuse
	*/
	override func prepareForReuse() {
		super.prepareForReuse()
		
		if activity == nil {
			activity = AnimationView(animation: Animation.named(Constants.activity.rawValue))
			activity?.contentMode = .scaleAspectFit
			activity?.frame = imageApplication?.frame ?? contentView.frame
			
			if let unwrappedActivityView = activity {
				contentView.addSubview(unwrappedActivityView)
			}
			activity?.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		}
		activity?.alpha = 1.0
		
		imageApplication?.layer.cornerRadius = 6.0
		imageApplication?.alpha = 0.0
		imageApplication?.image = nil
		labelName?.text = ""
		labelSummary?.text = ""
		labelAppStoreLink?.text = ""
	}
	
	/**
		Set image
	*/
	func setImage() {
		UIView.animate(withDuration: 0.3, animations: { [weak self] in
			self?.imageApplication?.alpha = 1.0
			self?.activity?.alpha = 0.0
		})
	}
}
