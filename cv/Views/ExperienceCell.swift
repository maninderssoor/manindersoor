//
//  ExperienceCell.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	ExperienceCell
*/
final class ExperienceCell: UITableViewCell {
	
	// MARK: Identifier
	static let identifier = "ExperienceCell"
	
	// MARK: Outlets
	
	///	The title of the job
	@IBOutlet weak var labelTitle: UILabel?
	
	///	The Dates for the job
	@IBOutlet weak var labelDate: UILabel?
	
	///	The organisation
	@IBOutlet weak var labelOrganisation: UILabel?
	
	///	A summary of the role
	@IBOutlet weak var labelSummary: UILabel?
	
	///	A horizontally scrolling collection of tags
	@IBOutlet weak var collectionTags: UICollectionView?
	
	// MARK: Setup
	
	/**
		Prepare for reuse
	*/
	override func prepareForReuse() {
		super.prepareForReuse()
		
		labelTitle?.text = ""
		labelDate?.text = ""
		labelOrganisation?.text = ""
		labelSummary?.text = ""
		collectionTags?.reloadData()
	}
	
}
