//
//  HeaderView.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	HeaderView
*/
final class HeaderView: UITableViewHeaderFooterView {
	
	// MARK: Identifier
	static let identifier = "HeaderView"
	
	// MARK: Outlets
	
	///	The title
	@IBOutlet weak var labelTitle: UILabel?
	
	// MARK: Setup
	
	/**
		Prepare for reuse
	*/
	override func prepareForReuse() {
		super.prepareForReuse()
		
		labelTitle?.text = ""
	}
}
