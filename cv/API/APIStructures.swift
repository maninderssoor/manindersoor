//
//  APIStructure.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	An API Progress object when the API call conforms to APIProtocol
*/
struct APIProtocolCompletion {
	
	/// If the call was a success
	let isSuccess: Bool
	
	/// Decoded JSON object being returned
	let service: APIProtocol?
	
	/// The full HTTP Response
	let httpResponse: HTTPURLResponse?
	
	/// The HTTP Response Status Code
	let httpResponseCode: Int?
	
	/// If there was an error, an error object
	let error: Error?
	
	/**
		Basic Initialiser
	*/
	init(isSuccess: Bool, service: APIProtocol? = nil, httpResponse: HTTPURLResponse? = nil, httpResponseCode: Int? = nil, error: Error? = nil) {
		self.isSuccess = isSuccess
		self.service = service
		self.httpResponse = httpResponse
		self.httpResponseCode = httpResponseCode
		self.error = error
	}
}
