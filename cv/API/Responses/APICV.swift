//
//  GenericResponse.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APICV Type
*/
struct APICV: Decodable {
	
	///	The name
	let name: String
	
	///	Website of this person
	let website: String
	
	///	LinkedIn Link for this person
	let linkedIn: String
	
	///	A summary of the person
	let summary: String
	
	///	An array of skills
	let skills: [String]
	
	///	Experiences
	let experience: [APIExperience]
	
	///	Applications
	let applications: [APIApplication]
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case name
		case website
		case linkedIn = "linkedin"
		case summary
		case skills
		case experience
		case applications
	}
}
