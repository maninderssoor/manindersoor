//
//  APIExperience.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APIExperience Type
*/
struct APIExperience: Decodable {
	
	///	The title held
	let title: String
	
	///	The start date
	let startDate: Int
	
	///	The end date
	let endDate: Int
	
	///	Name of the organisation/ company
	let organisation: String
	
	///	Summary of experience
	let summary: String
	
	///	Tags
	let tags: [String]
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case title
		case startDate = "start_date"
		case endDate = "end_date"
		case organisation
		case summary
		case tags
	}
}
