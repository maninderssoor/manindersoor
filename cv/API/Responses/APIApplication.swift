//
//  APIApplication.swift
//  cv
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	APIApplication Type
*/
struct APIApplication: Decodable {
	
	///	The name of the application
	let name: String
	
	///	Summary of the application
	let summary: String
	
	///	Logo url for the application
	let logoURL: String
	
	///	iTunes Link for the application
	let itunesURL: String
	
	///	An array of Gallery URLs
	let gallery: [String]
	
	/**
		CodingKeys
	*/
	enum CodingKeys: String, CodingKey {
		case name
		case summary
		case logoURL = "logo_url"
		case itunesURL = "itunes_link"
		case gallery
	}
}
