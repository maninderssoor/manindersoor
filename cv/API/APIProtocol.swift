//
//  APIProtocol.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	A protocol to handle API objects as part of the Networking Library.

	Defines base properties needed to make the API call.
*/
protocol APIProtocol : Decodable {
	
	/// The name of this API call, for logging
	static var title: String { get }
	
	/// The URL for this API call
	static var urlString: String { get }
}
