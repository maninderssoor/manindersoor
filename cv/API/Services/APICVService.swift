//
//  GenericService.swift
//  TripPlanner
//
//  Created by Maninder Soor on 26/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

/**
	Creates the CV API Service
*/
struct APICVService: APIProtocol {
	
	/// The name of this API call, for logging
	static var title: String = "GET CV"
	
	/// The URL for this call
	static var urlString: String = "\(Constants.hostURL.rawValue)assets/cv.json"

	///	An array of CV items
	let cvs: [APICV]
	
	/**
		CodingKeys for the APICVService
	*/
	enum CodingKeys: String, CodingKey {
		case cvs = "cv"
	}
}
