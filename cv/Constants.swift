//
//  Constants.swift
//  LastFM
//
//  Created by Maninder Soor on 19/03/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation

enum Constants: String {
	
	/// Host name
	case hostURL = "http://manindersoor.com/"
	
	///	Lottie activity file
	case activity = "activity"
}


enum UITests: String {
	
	///	Table view
	case table
	
	///	The activity loader
	case activity
}
