//
//  GalleryViewController.swift
//  CV
//
//  Created by Maninder Soor on 17/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	A Gallery View controller
*/
final class GalleryViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
	
	// MARK: Identifiers
	
	///	Storyboard identifier
	static let identifier = "GalleryViewController"
	
	// MARK: Variables
	
	///	Summary view model
	var viewModel: GalleryViewModel?
	
	// MARK: UI
	
	///	The collection view
	@IBOutlet weak var collection: UICollectionView?
	
	// MARK: Lifecycle
	
	/**
		Setup the view
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupCollectionView()
		setupNavigation()
		collection?.reloadData()
	}
	
	// MARK: Setup
	
	/**
		Setup the view model
	*/
	func setup(withApplication application: Application) {
		self.viewModel = GalleryViewModel(application: application)
		self.navigationItem.title = viewModel?.title()
	}
	
	/**
		Setup collection view
	*/
	func setupCollectionView() {
		collection?.dataSource = self
		collection?.delegate = self
		
		collection?.register(UINib(nibName: GalleryCell.identifier, bundle: nil), forCellWithReuseIdentifier: GalleryCell.identifier)
	}
	
	/**
		Setup the navigation button
	*/
	func setupNavigation() {
		
		let backButton = UIButton(type: .custom)
		backButton.frame = CGRect(x: 0, y: 0, width: 40, height: 24)
		backButton.addTarget(self, action: #selector(goBack), for: .touchUpInside)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .normal)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .highlighted)
		backButton.setImage(#imageLiteral(resourceName: "Arrow-Back"), for: .disabled)
		backButton.setTitle(nil, for: .normal)
		backButton.setTitle(nil, for: .highlighted)
		backButton.setTitle(nil, for: .disabled)
		
		let leftBarButton = UIBarButtonItem(customView: backButton)
		navigationItem.leftBarButtonItem = leftBarButton
	}
	
	/**
		Go back
	*/
	@IBAction public func goBack() {
		navigationController?.popViewController(animated: true)
	}
	
	// MARK: UICollectionView Datasource
	
	/**
		Number of items
	*/
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return viewModel?.numberOfItems() ?? 0
	}
	
	/**
		Cell for item at index path
	*/
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GalleryCell.identifier, for: indexPath) as? GalleryCell else {
			preconditionFailure("Couldn't dequeue GalleryCell for index path \(String(describing: indexPath))")
		}
		
		cell.imageApplication?.sd_cancelCurrentImageLoad()
		if let urlString = viewModel?.imageURL(atIndexPath: indexPath) {
			cell.imageApplication?.sd_setImage(with: URL(string: urlString), completed: { (_, _, _, _) in
				cell.setImage()
			})
		}
		
		return cell
	}
	
	/**
		Size for item
	*/
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: view.frame.size.width,
					  height: view.frame.size.height)
	}
}


