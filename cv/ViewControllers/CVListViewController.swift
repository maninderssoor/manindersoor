//
//  ViewController.swift
//  MovieDB
//
//  Created by Maninder Soor on 03/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import UIKit
import Lottie
import SDWebImage

/**
	The CV View controller
*/
final class CVViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
	
	// MARK: Identifiers
	
	///	Storyboard identifier
	static let identifier = "CVViewController"
	
	// MARK: Variables
	
	///	View Model
	private let viewModel = CVViewModel()
	
	///	Summary view model
	private var summaryViewModel: SummaryViewModel?
	
	///	Skills view model
	private var skillsViewModel: SkillsViewModel?
	
	///	Skills tags
	private var skillsDatasource: TagsDataSource?
	
	///	An experience view model
	private var experienceViewModel: ExperienceViewModel?
	
	///	An Application View Model
	private var applicationViewModel: ApplicationViewModel?
	
	///	Activity animation view
	let activity = AnimationView()
	
	// MARK: UI
	
	///	The table view
	@IBOutlet weak var table: UITableView?
	
	// MARK: Lifecycle
	
	/**
		Setup the view
	*/
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupLoadingData(isLoading: true)
		setupTableView()
		setupLoadingView()
		loadData()
	}
	
	// MARK: Setup
	
	/**
		Setup table view
	*/
	func setupTableView() {
		table?.dataSource = self
		table?.delegate = self
		
		table?.register(UINib(nibName: HeaderView.identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: HeaderView.identifier)
		table?.register(UINib(nibName: SummaryCell.identifier, bundle: nil), forCellReuseIdentifier: SummaryCell.identifier)
		table?.register(UINib(nibName: SkillCell.identifier, bundle: nil), forCellReuseIdentifier: SkillCell.identifier)
		table?.register(UINib(nibName: ExperienceCell.identifier, bundle: nil), forCellReuseIdentifier: ExperienceCell.identifier)
		table?.register(UINib(nibName: ApplicationCell.identifier, bundle: nil), forCellReuseIdentifier: ApplicationCell.identifier)
		table?.accessibilityIdentifier = UITests.table.rawValue
	}
	
	/**
		Setup loading view
	*/
	func setupLoadingView() {
		activity.animation = Animation.named(Constants.activity.rawValue)
		activity.accessibilityIdentifier = UITests.activity.rawValue
		activity.contentMode = .scaleAspectFit
		activity.frame = view.frame
		view.addSubview(activity)
		activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
	}
	
	/**
		Animation for loading
	*/
	func setupLoadingData(isLoading loading: Bool, completion: ((Bool) -> Void)? = nil) {
		UIView.animate(withDuration: 0.3, delay: 0.0, options: .beginFromCurrentState, animations: { [weak self] in
			
			self?.activity.alpha = loading ? 1.0 : 0.0
			self?.table?.alpha = loading ? 0.0 : 1.0
			
		}) { (isComplete) in
			completion?(isComplete)
		}
		
		if loading {
			activity.play(fromProgress: 0, toProgress: 1, loopMode: LottieLoopMode.loop, completion: nil)
		} else {
			activity.pause()
		}
	}
	
	/**
		Load the data
	*/
	func loadData(completion: ((Bool) -> Void)? = nil) {
		viewModel.loadData { [weak self] (isSuccess) in
			
			guard let self = self, isSuccess else {
				print("No weak self or error loading data!!!")
				return
			}
			
			self.table?.reloadData()
			self.setupLoadingData(isLoading: false)
			
			if let cv = self.viewModel.fetchCV() {
				let skillsViewModel = SkillsViewModel(cv: cv)
				self.summaryViewModel = SummaryViewModel(cv: cv)
				self.skillsViewModel = skillsViewModel
				self.skillsDatasource = TagsDataSource(tags: skillsViewModel.skills())
				self.experienceViewModel = ExperienceViewModel(cv: cv)
				self.applicationViewModel = ApplicationViewModel(cv: cv)
			}
		}
	}
	
	// MARK: UITableView Datasource
	
	/**
		Number of sections
	*/
	func numberOfSections(in tableView: UITableView) -> Int {
		return viewModel.numberOfSections()
	}
	
	/**
		Number of items
	*/
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.numberOfItems(inSection: section)
	}
	
	/**
		Cell for item at index path
	*/
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.section {
		case CVSection.summary.rawValue:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: SummaryCell.identifier, for: indexPath) as? SummaryCell else {
				preconditionFailure("Couldn't dequeue SummaryCell at indexPath \(String(describing: indexPath)). Crashing!")
			}
			
			cell.labelName?.text = summaryViewModel?.name()
			cell.labelWebsite?.text = summaryViewModel?.website()
			cell.labelLinkedIn?.text = summaryViewModel?.linkedIn()
			cell.labelSummary?.text = summaryViewModel?.summary()
			
			return cell
		case CVSection.skills.rawValue:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: SkillCell.identifier, for: indexPath) as? SkillCell else {
				preconditionFailure("Couldn't dequeue SkillCell at indexPath \(String(describing: indexPath)). Crashing!")
			}
			
			cell.collectionSkills?.dataSource = skillsDatasource
			cell.collectionSkills?.delegate = skillsDatasource
			
			return cell
		case CVSection.experience.rawValue:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: ExperienceCell.identifier, for: indexPath) as? ExperienceCell else {
				preconditionFailure("Couldn't dequeue ExperienceCell at indexPath \(String(describing: indexPath)). Crashing!")
			}
			
			cell.labelTitle?.text = experienceViewModel?.title(atIndexPath: indexPath)
			cell.labelDate?.text = experienceViewModel?.date(atIndexPath: indexPath)
			cell.labelOrganisation?.text = experienceViewModel?.organisation(atIndexPath: indexPath)
			cell.labelSummary?.text = experienceViewModel?.summary(atIndexPath: indexPath)
			
			return cell
		case CVSection.applications.rawValue:
			guard let cell = tableView.dequeueReusableCell(withIdentifier: ApplicationCell.identifier, for: indexPath) as? ApplicationCell else {
				preconditionFailure("Couldn't dequeue ApplicationCell at indexPath \(String(describing: indexPath)). Crashing!")
			}
			
			cell.labelName?.text = applicationViewModel?.name(atIndexPath: indexPath)
			cell.labelSummary?.text = applicationViewModel?.summary(atIndexPath: indexPath)
			cell.labelAppStoreLink?.text = applicationViewModel?.appStoreURL(atIndexPath: indexPath)
			cell.imageApplication?.sd_cancelCurrentImageLoad()
			if let urlString = applicationViewModel?.imageURL(atIndexPath: indexPath) {
				cell.imageApplication?.sd_setImage(with: URL(string: urlString), completed: { (_, _, _, _) in
					cell.setImage()
				})
			}
			
			return cell
		default:
			preconditionFailure("Couldn't figure out which section you're showing. CraSHINg!")
		}
	}
	
	/**
		Height for header
	*/
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return viewModel.heightForHeader()
	}
	
	/**
		View for header
	*/
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: HeaderView.identifier) as? HeaderView else {
			print("Error getting the HeaderView")
			return nil
		}
		
		headerView.labelTitle?.text = viewModel.title(forSection: section)
		return headerView
	}
	
	// MARK: UITableViewDelegate
	
	/**
		Did select item
	*/
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		
		guard indexPath.section == CVSection.applications.rawValue else {
			return
		}
		
		guard let viewController = UIStoryboard(name: GalleryViewController.identifier, bundle: nil).instantiateInitialViewController() as? GalleryViewController else {
			return
		}
		
		guard let application = applicationViewModel?.application(atIndexPath: indexPath) else {
			return
		}
		
		viewController.setup(withApplication: application)
		navigationController?.pushViewController(viewController, animated: true)
	}
}

