//
//  NavigationController.swift
//  StackOverflow
//
//  Created by Maninder Soor on 10/04/2019.
//  Copyright © 2019 Maninder Soor. All rights reserved.
//

import Foundation
import UIKit

/**
	Navigation Controller
*/
class NavigationController: UINavigationController {
	
	/**
		Set the status bar colour
	*/
	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
}
